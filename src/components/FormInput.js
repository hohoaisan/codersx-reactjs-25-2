import React, { Component } from 'react';
import PropTypes from 'prop-types';

function FormInput(props) {
    let {label, inputName} = props;
    return (
      <div class="FormInput form-group">
        <label for={inputName}>{label}</label>
        <input 
        class="form-control form-control-sm" name={inputName} />
      </div>
    );
}

FormInput.propTypes  = {
  label: PropTypes.string,
  inputName: PropTypes.string,
  value: PropTypes.string,
}


export default FormInput;