import React from "react";

import CartItem from "./CartItem";

export default {
  title: "Cart/CartItem",
  component: CartItem,
};


const Template = (args) => <CartItem {...args} />;

export const RegularCartItem = Template.bind({});
RegularCartItem.args = {
  id: "1",
  quantity: 4,
  title: "Example title",
  description: "lorem jaja haid uau aab jaa",
};