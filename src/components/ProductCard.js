import React, { Component } from 'react';
import PropTypes from 'prop-types';

function ProductCard(props) {
    let { id, imgUrl, title, description } = props;
    return (
      <div className="card">
        <img style={{ height: 100 + 'px', objectFit: 'cover' }} className="card-img-top" src={imgUrl} alt={title}/>
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <button id={id} class="btn btn-primary btn-sm">
            Add to cart
          </button>
        </div>
      </div>
    );
}

ProductCard.propTypes = {
  id: PropTypes.string,
  imgUrl: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
};
export default ProductCard;
