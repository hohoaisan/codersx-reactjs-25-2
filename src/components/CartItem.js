import React, { Component } from 'react';
import PropTypes from 'prop-types';

function CartItem(props) {
    let { id, title, description, quantity} = props;
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <p className="card-text">
            Số lượng<span>{quantity}</span>
          </p>
          <button className="btn btn-primary">Xoá</button>
        </div>
      </div>
    );
}

CartItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  quantity: PropTypes.number
};

export default CartItem;
