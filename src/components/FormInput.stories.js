import React from "react";

import FormInput from "./FormInput";

export default {
  title: "Form/FormInput",
  component: FormInput,
};


const Template = (args) => <FormInput {...args} />;

export const RegularFormInput = Template.bind({});
RegularFormInput.args = {
  label: "Example label",
  inputName: "input-form-name",
  value: "Example name",
};