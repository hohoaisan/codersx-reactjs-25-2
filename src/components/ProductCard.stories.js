import React from "react";

import ProductCard from "./ProductCard";

export default {
  title: "Product/ProductCard",
  component: ProductCard,
};


const Template = (args) => <ProductCard {...args} />;

export const RegularProductCard = Template.bind({});
RegularProductCard.args = {
  id: "1",
  imgUrl: "https://via.placeholder.com/300",
  title: "Example title",
  description: "lorem jaja haid uau aab jaa",
};